Sistema de Análise de Crédito
==========================
Atualmente existe uma versão deste sistema rodando no seguinte endereço:
https://credit-analysis-hbsrm.herokuapp.com/

Para rodar local em sua máquina
==========================
- git clone https://heitorbernardino@bitbucket.org/heitorbernardino/srm-credit-analysis-app.git
- npm install
- ng serve
- Acessar o endereço http://localhost:4200

Tecnologias utilizadas
==========================
- Angular 6 (https://angular.io)
- Typescript (https://www.typescriptlang.org)
- Angular CLI (https://cli.angular.io)
- Bootstrap 3 (https://getbootstrap.com/docs/3.3/)