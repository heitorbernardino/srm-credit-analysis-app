import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { Routes } from '@angular/router';
import { CustomersListComponent } from './customers/customers-list/customers-list.component';
import { CustomersDetailsComponent } from './customers/customers-details/customers-details.component';

const appRoutes: Routes = [
    { path: '', component: CustomersListComponent },
    { path: 'customers/:id/details', component: CustomersDetailsComponent },
    { path: 'customers/new', component: CustomersDetailsComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}