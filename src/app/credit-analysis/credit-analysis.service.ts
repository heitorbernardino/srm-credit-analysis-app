import { Injectable } from '@angular/core';
import { CreditAnalysis } from './credit-analysis.model';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { ToastrManager } from 'ng6-toastr-notifications';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class CreditAnalysisService {

  private creditAnalysisUrl = `${environment.serverUrl}/credit-analysis`;

  creditAnalysis: CreditAnalysis[] = [];

  constructor(
    private http: HttpClient,
    public toastr: ToastrManager
  ) { }

  getCredityAnalysisByCustomer(customerId: number): Observable<CreditAnalysis[]> {
    return this.http.get<CreditAnalysis[]>(`${this.creditAnalysisUrl}/customer/${customerId}`);
  }

  updateCredityAnalysisList(customerId: number): void {
    this.http.get<CreditAnalysis[]>(`${this.creditAnalysisUrl}/customer/${customerId}`)
    .subscribe((creditAnalysis) => this.creditAnalysis = creditAnalysis);
  }

  getCredityAnalysisById(id: number): Observable<CreditAnalysis> {
    const url = `${this.creditAnalysisUrl}/${id}`;
    return this.http.get<CreditAnalysis>(url);
  }

  saveCreditAnalysis (creditAnalysis: CreditAnalysis): void {
    this.http.post(this.creditAnalysisUrl, creditAnalysis, httpOptions)
    .subscribe(
      (creditAnalysis) => {
        this.updateCredityAnalysisList((<CreditAnalysis>creditAnalysis).customer.id);
        this.toastr.successToastr('Dados da análise salvos com sucesso!', 'Muito bom!');
      },
      () => {
        this.toastr.errorToastr('Ocorreu um erro ao salvar a análise de crédito!', 'Atenção!')
      }
        
    );
  }

  deleteCreditAnalysis(creditAnalysisId: number): void {
    const url = `${this.creditAnalysisUrl}/${creditAnalysisId}`;
    this.http.delete(url, httpOptions)
    .subscribe((creditAnalysis) => this.updateCredityAnalysisList((<CreditAnalysis>creditAnalysis).customer.id));
  }

  cleanCredityAnalysisList(): void {
    this.creditAnalysis = [];
  }
}
