import { Customer } from "../customers/customer.model";
import { InterestRate } from "../interest-rates/interest-rate.model";

export class CreditAnalysis {
    id: number;
    customer = new Customer();
    interestRate = new InterestRate();
    riskType: string;
    interestTax: number;
    creditLimit: number;
    interestAmmount: number;
    creditLimmitWithTax: number;
    active:boolean;
}