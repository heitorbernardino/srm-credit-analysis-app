import { Component, OnInit, Input } from '@angular/core';
import { CreditAnalysis } from '../credit-analysis.model';
import { Customer } from 'src/app/customers/customer.model';
import { CreditAnalysisService } from '../credit-analysis.service';
import { InterestRate } from 'src/app/interest-rates/interest-rate.model';
import { InterestRatesService } from 'src/app/interest-rates/interest-rates.service';
import { ToastrManager } from 'ng6-toastr-notifications';

@Component({
  selector: 'app-credit-analysis-modal',
  templateUrl: './credit-analysis-form.component.html'
})
export class CreditAnalysisFormComponent implements OnInit {

  @Input() customerId: number;

  creditAnalysis: CreditAnalysis;

  interestRates: InterestRate[];

  constructor(
    private creditAnalysisService: CreditAnalysisService,
    private interestRateService: InterestRatesService,
    public toastr: ToastrManager
  ) { }

  ngOnInit() {
    this.newCreditAnalysis();
    this.interestRateService.getInterestRates()
    .subscribe(
      (interestRates) => this.interestRates = interestRates,
      () => {
         this.toastr.errorToastr('Ocorreu um erro ao acessar o servidor!', 'Atenção!');
      });
  }

  newCreditAnalysis() {
    this.creditAnalysis = new CreditAnalysis();
    this.creditAnalysis.interestRate = null;
  }

  saveAnalysis() {
    this.createCustomer();
    this.creditAnalysisService.saveCreditAnalysis(this.creditAnalysis);
  }

  private createCustomer() {
    const customer: Customer = new Customer();
    customer.id = this.customerId;
    this.creditAnalysis.customer = customer;
  }

  public get newCustomer(): boolean
  {
      return !this.customerId;
  }
}
