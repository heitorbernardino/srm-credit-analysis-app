import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { InterestRate } from './interest-rate.model';
import { environment } from 'src/environments/environment';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class InterestRatesService {

  private interestRateUrl = `${environment.serverUrl}/interest-rates`;

  constructor(
    private http: HttpClient
  ) { }

  getInterestRates(): Observable<InterestRate[]> {
    return this.http.get<InterestRate[]>(this.interestRateUrl);
  }
}
