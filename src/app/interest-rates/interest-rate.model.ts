export class InterestRate {
    id: number;
    code: string;
    description: string;
    tax: number;
}