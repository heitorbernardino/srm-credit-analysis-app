export class Customer {
    id: number;
    name:string;
    identification: string;
    active:boolean;
}