import { Component, OnInit } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomersService } from '../customers.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-customers-list',
  templateUrl: './customers-list.component.html'
})
export class CustomersListComponent implements OnInit {

  customers: Customer[];

  constructor(
    private customerService: CustomersService,
    private router: Router
  ) { }

  ngOnInit() {
    this.customerService.getCustomers()
    .subscribe(customers => this.customers = customers);
  }

  onSelect(customer: Customer): void {
    this.router.navigate(['/customers', customer.id, 'details']);
  }

  newCustomer(): void {
    this.router.navigate(['/customers', 'new']);
  }
}
