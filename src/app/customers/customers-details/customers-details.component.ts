import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { Customer } from '../customer.model';
import { CustomersService } from '../customers.service';
import { Subscription } from 'rxjs';
import { ActivatedRoute, Router } from '@angular/router';
import { CreditAnalysisService } from 'src/app/credit-analysis/credit-analysis.service';
import { ToastrManager } from 'ng6-toastr-notifications';
import { CreditAnalysis } from 'src/app/credit-analysis/credit-analysis.model';

@Component({
  selector: 'app-customers-details',
  templateUrl: './customers-details.component.html',
  styleUrls: ['./customers-details.component.css']
})
export class CustomersDetailsComponent implements OnInit {

  customer: Customer;
  subscription: Subscription;
  creditAnalysisToDelete: CreditAnalysis;

  constructor(
    private route: ActivatedRoute,
    private customerService: CustomersService,
    public creditAnalysisService: CreditAnalysisService,
    public toastr: ToastrManager,
    private router: Router
  ) {}

  ngOnInit() {
    this.subscription = this.route.params.subscribe(
      (params: any) => {
        const id = params['id'];

        if (id == null) {
          this.customer = new Customer();
          return;
        }

        this.customerService.getCustomer(id)
        .subscribe(customer => this.customer = customer);

        this.creditAnalysisService.updateCredityAnalysisList(id);

      }
    );
  }

  save(): void {
    this.customerService.saveCustomer(this.customer)
      .subscribe((customer) => this.customer = customer);

    this.toastr.successToastr('Dados do cliente salvos com sucesso!', 'Muito bom!');
  }

  prepareToDelete(creditAnalysis: CreditAnalysis): void {
    this.creditAnalysisToDelete = creditAnalysis;
  }

  deleteCreditAnalysis(): void {
    this.creditAnalysisService.deleteCreditAnalysis(this.creditAnalysisToDelete.id);

    this.toastr.successToastr('Análise de crédito removida com sucesso!', 'Muito bom!');

    this.creditAnalysisToDelete = null;
  }

  activateCreditAnalysis(creditAnalysis: CreditAnalysis) {
    creditAnalysis.active = true;
    this.creditAnalysisService.saveCreditAnalysis(creditAnalysis);
  }

  inactivateCreditAnalysis(creditAnalysis: CreditAnalysis) {
    creditAnalysis.active = false;
    this.creditAnalysisService.saveCreditAnalysis(creditAnalysis);
  }

  voltar(): void {
    this.router.navigate(['']);
    this.creditAnalysisService.cleanCredityAnalysisList();
  }
}
