import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { CustomersListComponent } from './customers-list/customers-list.component';
import { CustomersService } from './customers.service';
import { CustomersDetailsComponent } from './customers-details/customers-details.component';
import { CreditAnalysisModule } from '../credit-analysis/credit-analysis.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule } from 'ng6-toastr-notifications';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CreditAnalysisModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot()
  ],
  declarations: [
    CustomersListComponent,
    CustomersDetailsComponent
  ],
  exports: [
    CustomersListComponent,
    CustomersDetailsComponent
  ],
  providers: [
    CustomersService
  ]
})
export class CustomersModule { }
